package com.vironit.api.models;

import java.util.HashSet;
import java.util.Set;

public class ProductAmountModel {

    private ProductModel product;

    private Integer amount;

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
