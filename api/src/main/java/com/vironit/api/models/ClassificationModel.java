package com.vironit.api.models;

import java.util.Objects;

public class ClassificationModel {

        private int idClassification;
        private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdClassification() {
        return idClassification;
    }

    public void setIdClassification(int idClassification) {
        this.idClassification = idClassification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClassificationModel)) return false;
        ClassificationModel that = (ClassificationModel) o;
        return idClassification == that.idClassification &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idClassification, name);
    }
}
