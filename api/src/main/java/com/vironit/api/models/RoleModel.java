package com.vironit.api.models;

import java.util.Objects;

public class RoleModel {



    private short idRole;
    private String nameRole;

    public RoleModel() {
    }

    public RoleModel(short idRole, String nameRole) {
        this.idRole = idRole;
        this.nameRole = nameRole;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoleModel)) return false;
        RoleModel roleModel = (RoleModel) o;
        return idRole == roleModel.idRole &&
                Objects.equals(nameRole, roleModel.nameRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRole, nameRole);
    }

    public short getIdRole() {
        return idRole;
    }

    public void setIdRole(short idRole) {
        this.idRole = idRole;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }
}
