package com.vironit.api.models;

public enum StatusEnum {

    created,
    approved,
    shipped,
    finshed,
    undefined;

    StatusEnum() {
    }

}