package com.vironit.api.models;

import java.util.Objects;

public class ProductModel {

    private Integer idProduct;
    private String name;
    private Integer amount;
    private String description;
    private Integer price;
    private ClassificationModel classificationByClassification;
    private Integer currentAmount;



    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ClassificationModel getClassificationByClassification() {
        return classificationByClassification;
    }

    public void setClassificationByClassification(ClassificationModel classificationByClassification) {
        this.classificationByClassification = classificationByClassification;
    }

    public Integer getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Integer currentAmount) {
        this.currentAmount = currentAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductModel)) return false;
        ProductModel that = (ProductModel) o;
        return Objects.equals(idProduct, that.idProduct) &&
                Objects.equals(name, that.name) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(description, that.description) &&
                Objects.equals(price, that.price) &&
                Objects.equals(classificationByClassification, that.classificationByClassification) &&
                Objects.equals(currentAmount, that.currentAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProduct, name, amount, description, price, classificationByClassification, currentAmount);
    }
}
