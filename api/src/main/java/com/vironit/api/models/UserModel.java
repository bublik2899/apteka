package com.vironit.api.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class UserModel {

    private int idUser;
    private String lastname;
    private String firstname;
    private String phoneNumber;
    private String email;
    private String password;
    private RoleModel roleByRole;
    private Set<OrderModel> orders = new HashSet<>();

    public UserModel() {
    }

    public UserModel(String lastname, String firstname, String phoneNumber, String email, String password, RoleModel roleByRole) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.roleByRole = roleByRole;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserModel)) return false;
        UserModel userModel = (UserModel) o;
        return idUser == userModel.idUser &&
                Objects.equals(lastname, userModel.lastname) &&
                Objects.equals(firstname, userModel.firstname) &&
                Objects.equals(phoneNumber, userModel.phoneNumber) &&
                Objects.equals(email, userModel.email) &&
                Objects.equals(password, userModel.password) &&
                Objects.equals(roleByRole, userModel.roleByRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, lastname, firstname, phoneNumber, email, password, roleByRole);
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleModel getRoleByRole() {
        return roleByRole;
    }

    public void setRoleByRole(RoleModel roleByRole) {
        this.roleByRole = roleByRole;
    }

    public Set<OrderModel> getOrders() {
        return orders;
    }

    public void setOrders(Set<OrderModel> orders) {
        this.orders = orders;
    }
}
