package com.vironit.api.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class OrderModel {
    private LocalDate date;
    @NotBlank
    @Size(min = 1, max=40)
    private String shippingAdress;
    @Size(max=1000)
    private String description;
    private int id;
    @NotNull
    private Set<ProductModel> products = new HashSet<>();
    private int finalPrice;
    @NotNull
    private UserModel user;
    private Set<ProductAmountModel> productsTransf = new HashSet<>();

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    private StatusEnum status;


    public Set<ProductAmountModel> getProductsTransf() {
        return productsTransf;
    }

    public void setProductsTransf(Set<ProductAmountModel> productsTransf) {
        this.productsTransf = productsTransf;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {

        this.date = date;
    }

    public String getShippingAdress() {
        return shippingAdress;
    }

    public void setShippingAdress(String shippingAdress) {
        this.shippingAdress = shippingAdress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
