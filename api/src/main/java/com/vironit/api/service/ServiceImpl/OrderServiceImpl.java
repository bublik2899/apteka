package com.vironit.api.service.ServiceImpl;

import com.vironit.api.models.OrderModel;
import com.vironit.api.models.StatusEnum;
import com.vironit.api.models.UserModel;
import com.vironit.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Autowired
    UserServiceImpl userService;

    @Override
    public List<OrderModel> getAll() {
        RestTemplate restTemplate = new RestTemplate();
        OrderModel[] OrderResponse = restTemplate.getForObject(backendServerUrl + "/order/orders",
                OrderModel[].class);
        return  OrderResponse == null ? Collections.emptyList() : Arrays.asList(OrderResponse);
    }

    @Override
    public OrderModel saveOrder(OrderModel order){
        UserModel user = userService.getUserByEmail(order.getUser().getEmail());
        order.setUser(user);
        System.out.println(order.getUser().getFirstname());
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl+"/order/", order, OrderModel.class).getBody();
    }

    @Override
    public OrderModel updateOrder(OrderModel order) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl+"/order/order", order, OrderModel.class).getBody();
    }

    @Override
    public List<OrderModel> findOrder(LocalDate start, LocalDate end, StatusEnum status, String lastname,
                                      String firstname) {

        RestTemplate restTemplate = new RestTemplate();
        OrderModel[] orderResponse =  restTemplate.getForObject(backendServerUrl + "/order/search?start="+
                start + "&end=" + end + "&status=" + status + "&last=" + lastname +"&first=" + firstname,
                OrderModel[].class);
        return  orderResponse == null ? Collections.emptyList() : Arrays.asList(orderResponse);
    }
}