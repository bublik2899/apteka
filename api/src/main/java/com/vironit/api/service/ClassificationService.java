package com.vironit.api.service;


import com.vironit.api.models.ClassificationModel;

import java.util.List;

public interface ClassificationService {

    List<ClassificationModel> findAll();

}
