package com.vironit.api.service;

import com.vironit.api.models.UserModel;

public interface UserService {

    public UserModel getUserByEmail(String email);

    public UserModel saveUser(UserModel user);

}
