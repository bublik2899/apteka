package com.vironit.api.service;

import com.vironit.api.models.OrderModel;
import com.vironit.api.models.StatusEnum;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {

    List<OrderModel> getAll();

    OrderModel saveOrder(OrderModel orderModel);

    OrderModel updateOrder(OrderModel orderModel);

    List<OrderModel> findOrder(LocalDate start, LocalDate end, StatusEnum status,String lastname, String firstname);

}
