package com.vironit.api.service.ServiceImpl;

import com.vironit.api.models.UserModel;
import com.vironit.api.service.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public UserModel getUserByEmail(String email){
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(backendServerUrl + "/user/getbyemail/?email=" + email,
                UserModel.class);

    }

    @Override
    public UserModel saveUser(UserModel user){

        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl+"/user/", user, UserModel.class).getBody();

    }
}
