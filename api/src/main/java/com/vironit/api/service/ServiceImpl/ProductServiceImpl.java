package com.vironit.api.service.ServiceImpl;

import com.vironit.api.models.ClassificationModel;
import com.vironit.api.models.ProductModel;
import com.vironit.api.service.ProductService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public List<ProductModel> findAll( int numberPage) {
        RestTemplate restTemplate = new RestTemplate();
        ProductModel[] ProductResponse = restTemplate.getForObject(backendServerUrl +
                        "/product/products/?numberPage=" + numberPage,
                ProductModel[].class);
        return ProductResponse == null ? Collections.emptyList() : Arrays.asList(ProductResponse);
    }



    @Override
    public List<ProductModel> searchProduct(String name,Integer id,Integer minPrice,Integer maxPrice) {
        RestTemplate restTemplate = new RestTemplate();
        ProductModel[] ProductResponse = restTemplate.getForObject(backendServerUrl
                        + "/product/search?name=" + name + "&id=" + id + "&min=" + minPrice + "&max=" + maxPrice,
                ProductModel[].class);
        return ProductResponse == null ? Collections.emptyList() : Arrays.asList(ProductResponse);
    }

    @Override
    public ProductModel getById(int id) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(backendServerUrl + "/product/getbyid/" + id, ProductModel.class);
    }


    @Override
    public List<ProductModel> sortProduct(Integer idClassif, Boolean asc, Boolean priceAsc, Boolean priceDesc) {
        RestTemplate restTemplate = new RestTemplate();
        ProductModel[] ProductResponse = restTemplate.getForObject(backendServerUrl
                        + "/product/sort?id=" + idClassif + "&asc=" + asc + "&pricemin=" + priceAsc + "&pricemax=" + priceDesc,
                ProductModel[].class);
        return ProductResponse == null ? Collections.emptyList() : Arrays.asList(ProductResponse);
    }
}
