package com.vironit.api.service.ServiceImpl;

import com.vironit.api.models.ClassificationModel;
import com.vironit.api.service.ClassificationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Service
public class ClassificationServiceImpl implements ClassificationService {

    @Value("${backend.server.url}")
    private String backendServerUrl;


    @Override
    public List<ClassificationModel> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        ClassificationModel[] ClassificationResponse = restTemplate.getForObject(backendServerUrl + "/classification/classifications/", ClassificationModel[].class);
        return ClassificationResponse == null ? Collections.emptyList() : Arrays.asList(ClassificationResponse);
    }
}
