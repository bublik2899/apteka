package com.vironit.api.service;

import com.vironit.api.models.ProductModel;

import java.util.List;

public interface ProductService {

    List<ProductModel> findAll(int numberPage);


    ProductModel getById(int id);

    List<ProductModel> searchProduct(String name,Integer id,Integer minPrice,Integer maxPrice);

    List<ProductModel> sortProduct(Integer idClassif, Boolean asc, Boolean priceAsc, Boolean priceDesc);

}
