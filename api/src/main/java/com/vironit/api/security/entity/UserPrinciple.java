package com.vironit.api.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vironit.api.models.RoleModel;
import com.vironit.api.models.UserModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserPrinciple implements UserDetails {

    private Integer idUser;
    private String lastname;
    private String firstname;
    private String phoneNumber;
    private String email;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    public UserPrinciple(Integer idUser, String lastname, String firstname, String phoneNumber, String email, Collection<? extends GrantedAuthority> authorities, String password) {
        this.idUser = idUser;
        this.lastname = lastname;
        this.firstname = firstname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.authorities = authorities;
        this.password = password;
    }

    public static UserPrinciple build(UserModel user) {
        RoleModel role = user.getRoleByRole();
        GrantedAuthority authority = new SimpleGrantedAuthority(role.getNameRole());
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(authority);
        return new UserPrinciple(
                user.getIdUser(),
                user.getLastname(),
                user.getFirstname(),
                user.getPhoneNumber(),
                user.getEmail(),
                authorities,
                user.getPassword()
        );
    }

    public Integer getIdUser() {
        return idUser;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
