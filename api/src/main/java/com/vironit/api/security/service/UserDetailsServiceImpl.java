package com.vironit.api.security.service;

import com.vironit.api.models.UserModel;
import com.vironit.api.security.entity.UserPrinciple;
import com.vironit.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService  {

    @Autowired
    UserService userService;


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {
            UserModel user = userService.getUserByEmail(email);
            user.setPassword(user.getPassword().trim());
            user.setPassword(user.getPassword());
        return UserPrinciple.build(user);
    }

}
