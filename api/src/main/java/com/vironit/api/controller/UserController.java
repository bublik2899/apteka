package com.vironit.api.controller;


import com.vironit.api.models.ProductModel;
import com.vironit.api.models.UserModel;
import com.vironit.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/apteka/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/getbyemail", method = RequestMethod.GET)
    public ResponseEntity<UserModel> getUserByEmail(@RequestParam(value = "email") String email){
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }
}
