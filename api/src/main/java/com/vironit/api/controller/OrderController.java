package com.vironit.api.controller;

import com.vironit.api.models.OrderModel;
import com.vironit.api.models.ProductModel;
import com.vironit.api.models.StatusEnum;
import com.vironit.api.security.entity.ResponseMessage;
import com.vironit.api.service.OrderService;
import com.vironit.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/apteka/order")
public class OrderController {

    @Autowired
    ProductService productService;

    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('manager')")
    public ResponseEntity<List<OrderModel>> getAllOrders() {

        return ResponseEntity.ok(orderService.getAll());

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('manager')")
    public ResponseEntity<?> updateProject( @RequestBody @Valid OrderModel order, BindingResult bindingResult){

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(orderService.updateOrder(order));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> saveProject( @RequestBody @Valid OrderModel order, BindingResult bindingResult) {
        Set<ProductModel> products = order.getProducts();
        ProductModel productModel;
        for (ProductModel p : products) {
            productModel = productService.getById(p.getIdProduct());
            if(p.getCurrentAmount() > productModel.getAmount()){
                return new ResponseEntity<>(new ResponseMessage("amount product more total amount"), HttpStatus.BAD_REQUEST);
            }
        }
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(orderService.saveOrder(order));
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('manager')")
    public List<OrderModel> findOrders(@RequestParam(value = "start")
                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                      @RequestParam(value = "end")
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end,
                                      @RequestParam(value = "status") StatusEnum status,
                                      @RequestParam(value = "last") String lastname,
                                      @RequestParam(value = "first") String firstname){

        if(start == null) start = LocalDate.of(1970,1,1);
        if(end == null) end = LocalDate.now().plusDays(1);
        if (status == null) status = StatusEnum.undefined;

        return orderService.findOrder(start, end,  status, lastname, firstname);

    }
}
