package com.vironit.api.controller;


import com.vironit.api.models.ProductModel;
import com.vironit.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/apteka/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<List<ProductModel>> getAllProducts(@RequestParam(value = "numberPage") int numberPage) {
        return ResponseEntity.ok(productService.findAll(numberPage));
    }



    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<List<ProductModel>> searchProduct(@RequestParam(value = "name",  required = false) String name,
                                                            @RequestParam(value = "id",  required = false) Integer id,
                                                            @RequestParam(value = "min",  required = false) Integer minPrice,
                                                            @RequestParam(value = "max",  required = false) Integer maxPrice){
        if(id == null) id = 0;
        if(minPrice==null) minPrice = 1;
        if (maxPrice==null) maxPrice = 1000000000;
        return  ResponseEntity.ok(productService.searchProduct(name,id,minPrice,maxPrice));
    }

    @RequestMapping(value = "/sort", method = RequestMethod.GET)
    public ResponseEntity<List<ProductModel>> sortProduct(@RequestParam(value = "id",  required = false) Integer idClassif,
                                                            @RequestParam(value = "asc",  required = false, defaultValue = "false") Boolean asc,
                                                            @RequestParam(value = "min",  required = false, defaultValue = "false") Boolean minPrice,
                                                            @RequestParam(value = "max",  required = false, defaultValue = "false") Boolean maxPrice){
        if(idClassif == null) idClassif = 0;


        return  ResponseEntity.ok(productService.sortProduct(idClassif ,asc ,minPrice,maxPrice));
    }



}
