package com.vironit.api.controller;


import com.vironit.api.models.ClassificationModel;
import com.vironit.api.models.ProductModel;
import com.vironit.api.service.ClassificationService;
import com.vironit.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/apteka/classification/")
public class ClassificationController {

    @Autowired
    private ClassificationService classificationsService;

    @RequestMapping(value = "/classifications", method = RequestMethod.GET)
    public ResponseEntity<List<ClassificationModel>> getAllClassification() {
        return ResponseEntity.ok(classificationsService.findAll());
    }


}
