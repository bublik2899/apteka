package com.vironit.api.controller;


import com.vironit.api.models.JwtResponse;
import com.vironit.api.models.RoleModel;
import com.vironit.api.models.UserModel;
import com.vironit.api.security.entity.LoginUser;
import com.vironit.api.security.entity.ResponseMessage;
import com.vironit.api.security.entity.SignUpUser;
import com.vironit.api.security.jwt.JwtProvider;
import com.vironit.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody @Valid LoginUser login, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(new ResponseMessage("Fail"),HttpStatus.BAD_REQUEST);
		}


		UserModel user = userService.getUserByEmail(login.getEmail());

		if (user.getEmail() == null){
			return new ResponseEntity<>(new ResponseMessage("Fail -> user does not exist!"),
					HttpStatus.BAD_REQUEST);
		}

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpUser signUp) {
		UserModel user = userService.getUserByEmail(signUp.getEmail());

		if(!signUp.getPassword().equals(signUp.getRepeatPassword())){
			return new ResponseEntity<>(new ResponseMessage("Passwords do not match"),
					HttpStatus.BAD_REQUEST);
		}

		if (user.getEmail() != null) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		UserModel user1 = new UserModel(signUp.getLastname(), signUp.getFirstname(), signUp.getPhoneNumber(), signUp.getEmail(),
				encoder.encode(signUp.getPassword()), new RoleModel((short) 2, "user"));


		userService.saveUser(user1);

		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);

	}

}