import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { JwtResponse } from './jwt-response';
import { Login } from './model/login'
import {SignUpUser} from "./model/SignUpUser";
//import { SignUpInfo } from './signup-info';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginUrl = 'http://localhost:8082/api/auth/signin';
  private signupUrl = 'http://localhost:8082/api/auth/signup';

  constructor(private http: HttpClient) {
  }

  logIn(credentials: Login): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  signUp(info: SignUpUser): Observable<string> {
    return this.http.post<string>(this.signupUrl, info, httpOptions);
  }
}
