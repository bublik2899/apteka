export class SignUpUser {

  lastname: string;
  firstname: string;
  phoneNumber: string;
  email: string;
  password: string;
  repeatPassword: string;

}
