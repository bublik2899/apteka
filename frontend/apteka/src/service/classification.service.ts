import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Classification} from "../model/Classification";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class ClassificationService {
  url = environment.url;
  constructor(private http: HttpClient){
  }

  getClassification(): Observable<Classification[]>{
    return this.http.get<Classification[]>(this.url + 'classification/classifications');
  }
  
}
