import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";
import {User} from "../model/User";

@Injectable({
  providedIn: 'root'
})

export class UserService {

  url = environment.url;
  constructor(private http: HttpClient){
  }

  getUserbyEmail(email: string): Observable<User>{
    return this.http.get<User>(this.url + 'user/getbyemail?email=' + email);
  }

}
