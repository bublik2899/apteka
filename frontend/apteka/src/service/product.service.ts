import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "../model/Product";
import {Injectable} from "@angular/core";
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = environment.url;
  constructor(private http: HttpClient){

  }

  getProduct(currentPage: number): Observable<Product[]>{
    return this.http.get<Product[]>(this.url +"product/products?numberPage=" + currentPage);
  }

  searchProducts(name: string, id: string, min: string, max: string): Observable<Product[]>{
    return this.http.get<Product[]>(this.url + "product/search?name=" + name + "&id=" + id + "&min=" + min + "&max=" + max);
  }

  getProductByClassif(id: string): Observable<Product[]>{
    return this.http.get<Product[]>(this.url + "product/getbyclassif?id=" + id);
  }

  deleteProduct(id: string): Observable<{}> {
    console.log(id);
    return this.http.delete<{}> (this.url + "product/delete/"+ id);

  }

  sortProduct(id: string, asc: boolean, priceMin: boolean, priceMax: boolean): Observable<Product[]>{
    return this.http.get<Product[]>(this.url + "product/sort?id=" + id + "&asc=" + asc + "&min=" + priceMin + "&max=" + priceMax );
  }
}
