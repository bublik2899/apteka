import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";
import {Order} from 'src/model/Order';
import {Product} from "../model/Product";

@Injectable({
  providedIn: 'root'
})

export class OrderService {
  url = environment.url;
  constructor(private http: HttpClient){
  }

  getAllOrders(): Observable<Order[]>{
    return this.http.get<Order[]>(this.url + 'order/orders');
  }

  saveOrder(order: Order): Observable<Order> {
    return this.http.post<Order>(this.url+ 'order/', order);
  }

  updateOrder(order: Order): Observable<Order>{
    return this.http.post<Order>(this.url + 'order/update', order);
  }

  searchOrders(start: string, end: string, status: string, last: string, first: string): Observable<Order[]>{
    return this.http.get<Order[]>(this.url + "order/search?start=" + start + "&end=" + end + "&status=" + status
      + "&last=" + last + "&first=" + first);
  }


}
