import {Injectable, EventEmitter} from "@angular/core";
import {Product} from "../model/Product";
import {ProductService} from "./product.service";

@Injectable({
  providedIn: 'root'
})
export class TransferService {


  constructor(private productService: ProductService){

}



  prod: Product[] = [];

  onClick:EventEmitter<Product[]> = new EventEmitter();

  public doClick(name: string, id: string, min: string, max: string): void{

      this.productService.searchProducts(name, id, min, max).subscribe( products =>{
      this.prod = products as Product[];
        this.onClick.emit(this.prod);
    });

    this.onClick.emit(this.prod);
  }


}
