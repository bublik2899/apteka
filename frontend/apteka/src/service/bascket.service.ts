import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BascketService {

  public deleteProductFromBasket(id: string): void {
    localStorage.removeItem(id);

  }

}
