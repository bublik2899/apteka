import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {BasketComponent} from "./home/basket/basket.component";
import {RegistrationComponent} from "./registration/registration.component";
import {UserAccountComponent} from "./userAccount/userAccount.component";
import {EmployeeComponent} from "./employee/employee.component";

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'basket', component: BasketComponent},
   {path: 'registration', component: RegistrationComponent},
  {path: 'user', component: UserAccountComponent},
  {path: 'employee', component: EmployeeComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
