import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HomeComponent} from "./home/home.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NavComponent} from "./home/nav/nav.component";
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormControl, FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal'
import {BasketComponent} from "./home/basket/basket.component";
import { httpInterceptorProviders } from "../auth/auth-interceptor";
import {RegistrationComponent} from "./registration/registration.component";
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";
import {UserAccountComponent} from "./userAccount/userAccount.component";
import {EmployeeComponent} from "./employee/employee.component";
import { TabsModule } from 'ngx-bootstrap/tabs';
import {AccordionModule, CollapseModule} from "ngx-bootstrap";
import { NgxLoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    BasketComponent,
    RegistrationComponent,
    UserAccountComponent,
    EmployeeComponent
  ],
  imports: [
    Ng4LoadingSpinnerModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    AccordionModule.forRoot(),
    CollapseModule.forRoot(),
    NgxLoadingModule.forRoot({})

  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
