import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {Product} from "../../../model/Product";
import {TransferService} from "../../../service/transfer.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ClassificationService} from "../../../service/classification.service";
import {Classification} from "../../../model/Classification";
import {Login} from "../../../auth/model/login";
import {AuthService} from "../../../auth/auth.service";
import {TokenStorageService} from "../../../auth/token-storage.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {Route, Router} from "@angular/router";

@Component({
  selector: 'nav-app',
  templateUrl: './nav.component.html',
  styles:['body .modal-lg {width: 150%;}']
})

export class NavComponent implements OnInit{

  private searchControl: FormControl;
  private loginControl: FormControl;
  private passwordControl: FormControl;
  private  productName: string ="";
  private idClassif: string ="";
  private minPrice: string="";
  private maxPrice: string ="";
  private products: Product[] = [];
  private modalRef: BsModalRef;
  private classifications: Classification[] = [];
  private login: Login;
  private email : string;
  private password : string;
  private errorMessage : string;
  private isLoginFailed : boolean = false;
  private isLoggedIn: boolean = false;
  private roles: string[] = [];
  private loading = false;

  constructor(private transService: TransferService,
              private classificationService: ClassificationService,
              private modalService: BsModalService,
              private authService: AuthService,
              private tokenStorage: TokenStorageService,
              private loadingService: Ng4LoadingSpinnerService,
              private router: Router){
  }

  ngOnInit(){

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }

    this.searchControl = new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(3), Validators.maxLength(100)]);
    this.searchControl.valueChanges.subscribe((value) => this.productName = value);

    this.loginControl = new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),Validators.email,
      Validators.minLength(4), Validators.maxLength(320)]);
    this.loginControl.valueChanges.subscribe((value) => this.email = value);


    this.passwordControl = new FormControl("");
    this.passwordControl.valueChanges.subscribe((value) => this.password = value);


    this.loadClassific();
  }

 private search(): void{
    this.transService.doClick(this.productName, this.idClassif, this.minPrice, this.maxPrice);
 }

 private loadClassific(): void{
    this.classificationService.getClassification().subscribe(classifications =>{
      this.classifications = classifications as Classification[];
    });
}

  private openModal(template: TemplateRef<any>, descriptions: string) {
    this.modalRef = this.modalService.show(template);
  }

  private  logOut(){
    this.tokenStorage.signOut();
    this.isLoggedIn = false;
  }

  private logIn() {

    this.loading = true;
    this.login = new Login(
      this.email,
      this.password);

    this.authService.logIn(this.login).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        console.log(this.tokenStorage.getToken());
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles =  this.tokenStorage.getAuthorities();
        if(this.roles[0] === "manager"){
          this.router.navigate(['/employee']);
        }
        this.modalRef.hide();
        this.loading = false;

      },
      error => {
        this.loading = false;
        console.log(error);
        this.errorMessage = error.error.message;
        console.log(this.errorMessage);
        let errorMessage1 = this.errorMessage.split('>');
        this.errorMessage = errorMessage1[1];
        if(this.errorMessage == " Unauthorized"){
          this.errorMessage = "Incorrect password"
        }
        this.isLoginFailed = true;

      }
    );
  }

  private reloadPage() {
    window.location.reload();
  }

  public closeModal(): void {
    this.modalRef.hide();
  }

  private onSelect(id: string): void {
    this.idClassif = id;
    console.log(id);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

}





