import {Component, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {Product} from "../../../model/Product";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {FormControl, Validators} from "@angular/forms";
import {Order} from "../../../model/Order";
import {OrderService} from "../../../service/order.service";
import {Subscription} from "rxjs";
import {TokenStorageService} from "../../../auth/token-storage.service";
import {Login} from "../../../auth/model/login";
import {AuthService} from "../../../auth/auth.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {User} from "../../../model/User";
import {Router} from "@angular/router";





@Component({
  selector: 'basket-app',
  templateUrl: './basket.component.html'
})
export class BasketComponent implements OnInit {

  @ViewChild('template', {static: false}) template: TemplateRef<any>;
  @ViewChild('template1', {static: false}) template1: TemplateRef<any>;
  private products: Product[] = [];

  modalRef: BsModalRef;

  adressControl: FormControl;
  descripControl: FormControl;
  amountControl: FormControl;
  private loginControl: FormControl;
  private passwordControl: FormControl;
  private email : string;
  private password : string;
  private errorMessage : string;
  private errorMessage1: string;
  private isLoginFailed : boolean = false;
  private login: Login;
  private user: User = new User();
  private flag: boolean = false;
  private isOrderFailed: boolean = false;

  order: Order = new Order();
  count: number = 0;
  amount: number = 0;

  private isLoggedIn: boolean = false;
  private subscriptions: Subscription[] = [];


  ngOnInit() {

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }
    this.loadProduct();
    this.countPrice();

    if(this.products.length == 0){
      this.flag = true;
    }

    this.adressControl = new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(3), Validators.maxLength(40)]);
    this.adressControl.valueChanges.subscribe((value) => this.order.shippingAdress = value);

    this.descripControl = new FormControl("", [ Validators.maxLength(1000)]);
    this.descripControl.valueChanges.subscribe((value) => this.order.description = value);

    this.loginControl = new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),Validators.email,
      Validators.minLength(3), Validators.maxLength(320)]);
    this.loginControl.valueChanges.subscribe((value) => this.email = value);

    this.passwordControl = new FormControl("");
    this.passwordControl.valueChanges.subscribe((value) => this.password = value);
  }

  constructor(private modalService: BsModalService,
              private orderService: OrderService,
              private tokenStorage: TokenStorageService,
              private authService: AuthService,
              private loadingService: Ng4LoadingSpinnerService,
              private router: Router
  ) {

  }

  public loadProduct(): void {
    for (let i = 0; i < localStorage.length; i++) {
      let key = localStorage.key(i);
      this.products[i] = JSON.parse(localStorage.getItem(key));
    }
  }

  public deleteProductFromBasket(id: string): void {
    localStorage.removeItem(id);
    this.update();
    this.countPrice();
    this.count = 0;
    this.countPrice();
    if(this.products.length == 0){
      this.flag = true;
    }
  }


  public update(): void {
    let products1: Product[] = [];
    for (let i = 0; i < localStorage.length; i++) {
      let key = localStorage.key(i);
      products1[i] = JSON.parse(localStorage.getItem(key));
    }
    this.products = products1;
  }

  private openModal() {
    let template3: TemplateRef<any>;
    if(!this.isLoggedIn) {template3 = this.template1;}
    else {template3 = this.template}
    this.modalRef = this.modalService.show(template3);
  }


  private createOrder(): void {
    this.user.email = this.tokenStorage.getUsername();
    this.order.user = this.user;
    this.order.products = this.products;
    this.order.finalPrice = this.count;
    this.loadingService.show();

    this.orderService.saveOrder(this.order).subscribe(() => {
      localStorage.clear();
      this.modalRef.hide();
      this.router.navigate(['/']);

      },
    error => {
      console.log(error);
      this.errorMessage1 = error.error.message;
      this.isOrderFailed = true;
    });

  }

  private countPrice(): void {
    this.count= 0;
    for (let i = 0; i < this.products.length; i++) {
      this.count = this.count + this.products[i].price*this.products[i].currentAmount;
    }
  }

  private logIn() {

    this.login = new Login(
      this.email,
      this.password);

    this.authService.logIn(this.login).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        console.log(this.tokenStorage.getToken());
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.loadingService.hide();
        this.modalRef.hide();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        let errorMessage1 = this.errorMessage.split('>');
        this.errorMessage = errorMessage1[1];
        if(this.errorMessage == " Unauthorized"){
          this.errorMessage = "Incorrect password"
        }
        this.isLoginFailed = true;

      }
    );
  }

}

