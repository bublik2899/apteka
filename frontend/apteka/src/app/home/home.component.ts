import {Component, OnInit, TemplateRef} from '@angular/core';
import {ClassificationService} from "../../service/classification.service";
import {Classification} from "../../model/Classification";
import {ProductService} from "../../service/product.service";
import {Product} from "../../model/Product";
import {TransferService} from "../../service/transfer.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Router} from '@angular/router';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',

})
export class HomeComponent implements OnInit{


  private classifications: Classification[] = [];
  private products: Product[] = [];
  modalRef: BsModalRef;
  private descriptions: string;
   aZ: boolean = false;
   priceMin: boolean = false;
   priceMax: boolean = false;
   idClassif: string = "";
   flag: boolean =false;
   currentPage : number = 1;

  ngOnInit(){
    this.loadClassification();
    this.loadProduct();
  }


  constructor(private classificationService: ClassificationService,
              private productService: ProductService,
              private trans: TransferService,
              private modalService: BsModalService){
    this.trans.onClick.subscribe(cnt => this.products = cnt);

  }


  private loadClassification(): void  {
    this.classificationService.getClassification().subscribe(classif => {
      this.classifications = classif as Classification[];

    });
  }

  nextPage(){

    if(this.products.length < 7){
      this.currentPage--;
    }
    this.currentPage++;
    this.loadProduct();
  }

  PreviosPage(){
      this.currentPage--;
      if(this.currentPage == 0){
        this.currentPage = 1;
      }
      console.log(this.currentPage);
      this.loadProduct();

  }

  private loadProduct(): void {
    this.productService.getProduct(this.currentPage).subscribe( product => {
      this.products = product as Product[];
    })
  }



  private addToBuscket(product: Product): void{
    let id : string = product.idProduct;
    product.currentAmount = 1;
    let p: string = JSON.stringify(product);
    localStorage.setItem(id, p);
  }


  private updateProduct(): void {
    this.loadProduct;
  }

  private openModal(template: TemplateRef<any>, descriptions: string) {
    this.modalRef = this.modalService.show(template);
    this.descriptions = descriptions;
  }

   sortProducts(id: string, aZ?: boolean, priceMin?: boolean, priceMax?: boolean) {

     this.idClassif = id;
     this.aZ = aZ;
     this.priceMin = priceMin;
     this.priceMax = priceMax;
     this.productService.sortProduct(this.idClassif, this.aZ, this.priceMin, this.priceMax).subscribe(products => {
       this.products = products as Product[];
     });
   }

   resetFilters(){
     this.aZ = false;
     this.priceMin = false;
     this.priceMax = false;
     this.idClassif = "";
     this.loadProduct();
   }

}
