import {Component, OnInit, TemplateRef} from "@angular/core";
import {OrderService} from "../../service/order.service";
import {Order} from "../../model/Order";
import {Product} from 'src/model/Product';
import {Status} from "../../model/Status";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ProductAmount} from "../../model/ProductAmount";
import {User} from "../../model/User";


@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
})

export class EmployeeComponent implements OnInit{

  orders: Order[] = [];
  products: ProductAmount[] = [];
  isOpen: boolean = false;
  isCollapsed = true;
  statuses : Status = new Status();
  user: User = new User();
  keys = Object.keys;
  private modalRef: BsModalRef;
  private last: string = "";
  private first: string = "";
  private end: string = "";
  private start: string = "";
  private status: string = "";
  private loading= false;


  constructor(private orderService: OrderService,
              private modalService: BsModalService){

  }
  ngOnInit(){
    this.loadOrder();
  }

  loadOrder(){
    this.orderService.getAllOrders().subscribe(orders =>{
      this.orders = orders as Order[];
    })
  }


  updateOrder(order: Order){
    this.orderService.updateOrder(order).subscribe( order =>{

    })
  }

  private openModal(template: TemplateRef<any>, products: ProductAmount[]) {
    this.products = products;
    this.modalRef = this.modalService.show(template);
  }

  private openModalUser(template: TemplateRef<any>, user: User) {
    this.user = user;
    this.modalRef = this.modalService.show(template);
  }

  private searchOrder(){
    this.loading = true;
    this.orderService.searchOrders(this.start, this.end, this.status, this.last, this.first).subscribe(orders => {
      this.orders = orders as Order[];
      this.loading = false;
    })
  }


}
