import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SignUpUser} from "../../auth/model/SignUpUser";
import {AuthService} from "../../auth/auth.service";
import { AbstractControl } from '@angular/forms';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import { Router }          from '@angular/router';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',

})



export class RegistrationComponent implements OnInit {

  @ViewChild('template', {static: false}) template: TemplateRef<any>;

  errorMessage : string;
  message: string;
  isSignedUp: boolean = false;
  isSignUpFailed: boolean = false;
  signUpUser: SignUpUser = new SignUpUser();
  modalRef: BsModalRef;
  edit : boolean = false;
  private loading = false;

  registrationForm : FormGroup = new FormGroup({

    "lastname": new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(1), Validators.maxLength(40)]),
    "firstname": new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(1), Validators.maxLength(40)]),
    "phoneNumber": new FormControl("+375", [Validators.required, Validators.pattern("[+]375[0-9]{9}"),
      Validators.minLength(1), Validators.maxLength(40)]),
    "email": new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(1), Validators.maxLength(320),Validators.email]),
        "password": new FormControl( "", [Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(6), Validators.maxLength(20)]),
    "passwordRepeat": new FormControl( "",[Validators.required, Validators.pattern(/^\S{0,50}$/),
      Validators.minLength(6), Validators.maxLength(20)])
  });


  constructor(private authService: AuthService,
              private modalService: BsModalService,
              private loadingService: Ng4LoadingSpinnerService,
              private router: Router){

  }



  ngOnInit() {

  }

  private saveUser(): void {

    this.loading= true;
    this.signUpUser.lastname = this.registrationForm.value.lastname;
    this.signUpUser.firstname = this.registrationForm.value.firstname;
    this.signUpUser.phoneNumber = this.registrationForm.value.phoneNumber;

    this.signUpUser.email = this.registrationForm.value.email;
    this.signUpUser.password = this.registrationForm.value.password;
    this.signUpUser.repeatPassword = this.registrationForm.value.passwordRepeat;

    this.authService.signUp(this.signUpUser).subscribe(

      data => {
        this.message = data;
        this.isSignedUp = true;
        this.isSignUpFailed = false;
        this.loadingService.hide();
        this.router.navigate(['/']);
        this.openModal(this.template,"s");
        this.loading=false;
      },
      error => {
        this.loading=false;
        console.log(error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  private openModal(template: TemplateRef<any>, descriptions: string) {
    this.modalRef = this.modalService.show(template);
  }

}
