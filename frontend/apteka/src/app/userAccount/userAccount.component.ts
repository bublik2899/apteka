import {Component, OnInit, TemplateRef} from '@angular/core';
import {UserService} from "../../service/user.service";
import {TokenStorageService} from "../../auth/token-storage.service";
import {User} from "../../model/User";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductAmount} from "../../model/ProductAmount";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: 'app-userAccount',
  templateUrl: './userAccount.component.html',
})

export class UserAccountComponent implements OnInit{

  private modalRef: BsModalRef;
  products: ProductAmount[] = [];
  email: string;
  user: User = new User();
  edit: boolean = true;
  date: Date;
  userForm: FormGroup;
  ngOnInit(){
    this.email= this.tokenStorage.getUsername();
    this.getUserByEmail();

    this.userForm = new FormGroup({

      "lastname": new FormControl({value: '', disabled: this.edit}, [Validators.required, Validators.pattern(/^\S{0,50}$/),
        Validators.minLength(1), Validators.maxLength(40)]),
      "firstname": new FormControl({value: '', disabled: this.edit}, [Validators.required, Validators.pattern(/^\S{0,50}$/),
        Validators.minLength(1), Validators.maxLength(40)]),
      "phoneNumber": new FormControl({value: '', disabled: this.edit}, [Validators.required, Validators.pattern("[+]375[0-9]{9}"),
        Validators.minLength(1), Validators.maxLength(40)]),
      "email": new FormControl({value: '', disabled: this.edit}, [Validators.required, Validators.pattern(/^\S{0,50}$/),
        Validators.minLength(1), Validators.maxLength(320), Validators.email]),
      "password": new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
        Validators.minLength(6), Validators.maxLength(20)]),
      "passwordRepeat": new FormControl("", [Validators.required, Validators.pattern(/^\S{0,50}$/),
        Validators.minLength(6), Validators.maxLength(20)])
    });
  }

  constructor(private userService: UserService,
              private tokenStorage: TokenStorageService,
              private modalService: BsModalService
  ){

  }


  getUserByEmail(){
    this.userService.getUserbyEmail(this.email).subscribe(user => {
      this.user =user;


      console.log(this.user.firstname);
    })

  }


  private openModal(template: TemplateRef<any>, products: ProductAmount[]) {
    this.products = products;
    this.modalRef = this.modalService.show(template);
  }

}
