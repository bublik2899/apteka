import {Classification} from "./Classification";

export class Product {

  idProduct: string;
  name: string;
  amount: string;
  classificationByClassification: Classification;
  description: string;
  price: number;
  currentAmount: number ;

}
