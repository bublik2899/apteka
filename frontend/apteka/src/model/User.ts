import {Order} from "./Order";

export class User {

  lastname: string;
  firstname: string;
  phoneNumber: string;
  email: string;
  orders: Order[];

}
