import {Product} from "./Product";

export class ProductAmount {

  product: Product;

  amount: number;
}
