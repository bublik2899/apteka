import {Product} from "./Product";
import {User} from "./User";
import {ProductAmount} from "./ProductAmount";

export class Order {

  id: string;
  date: string;
  shippingAdress: string;
  description: string;
  status: string;
  products: Product[];
  productsTransf: ProductAmount[];
  finalPrice : number;
  user: User;

}
