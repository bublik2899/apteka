<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Дмитрий
  Date: 17.06.2019
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr><th>Id</th><th>Name</th><th>Amount</th><th>Classification</th></tr><th></th>
    <c:forEach var="product" items="${products}">
        <tr>
            <td>${product.id_product}</td>
            <td>${product.name}</td>
            <td>${product.amount}</td>
            <td>${product.classification.name}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
