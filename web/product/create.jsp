<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Create product</title>
</head>
<body>
<h3>New product</h3>
<form method="post">
    <label>Name</label><br>
    <input name="name" maxlength="45"/><br><br>
    <label>Amount</label><br>
    <input name="amount" type="number" min="100"  /><br><br>
    <label>Classification</label><br><br>
    <select name="classific">
        <c:forEach var="classif" items="${classific}">
            <option value=${classif.idClassification}>${classif.name}</option>
        </c:forEach>
    </select>
    <br><br>
    <button type="submit">Add product</button>
</form>
</body>
</html>