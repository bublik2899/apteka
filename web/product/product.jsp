<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="<c:url value="/bootstrap-4.3.1/dist/css/bootstrap.min.css"/>" rel="stylesheet">

</head>
<body>
<div>
<p><a href='<c:url value="/product/create" />'>Create new</a></p>
        <form action='<c:url value="/product/search" />'>
            <div class="form-group row">
                <div class="col-4">
                    <label>Search by name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="col-8">
                    <div class="form-check" style="margin-top: 6%">
                        <input class="form-check-input" type="checkbox" id="gridCheck1" name="check1">
                        <label class="form-check-label" for="gridCheck1">
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-4">
                <select name="classific" id="select_" class="form-control">
                    <c:forEach var="classif" items="${classifications}">
                        <option value=${classif.idClassification}>
                                ${classif.name}
                        </option>
                    </c:forEach>
                </select>
                </div>
                <div class="col-8">
                    <div class="form-check" style="margin-top: 1%">
                        <input class="form-check-input" type="checkbox" id="gridCheck2" name="check2">
                        <label class="form-check-label" for="gridCheck2">
                        </label>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success" onclick="getValue()">Search</button>
        </form>

<table class="table">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Classification</th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach var="product" items="${products}">
        <tr>
            <td>${product.id_product}</td>
            <td>${product.name}</td>
            <td>${product.amount}</td>
            <td>${product.classification.name}</td>
            <td>
                <a href='<c:url value="/product/edit?id=${product.id_product}" />'>Edit</a>
            </td>
            <td>
                <form method="post" action='<c:url value="/product/delete" />' style="display:inline;">
                    <input type="hidden" name="id" value="${product.id_product}">
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>

        </tr>
    </c:forEach>
</table>
<script>
    function getValue() {
        var select = document.getElementById("select_");
        var value = select.value;
        document.location.href = "/sort?id=1";

    }
</script>
</div>
</body>

</html>