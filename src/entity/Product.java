package entity;

import java.io.Serializable;

public class Product implements Serializable {

    private int id;
    private String name;
    private int amount;
    private Classification classification;

    public Product(String name, int amount, Classification classification){
        this.name=name;
        this.amount=amount;
        this.classification=classification;
    }
    public Product(int id,String name, int amount, Classification classification){
        this.id=id;
        this.name=name;
        this.amount=amount;
        this.classification=classification;
    }

    public long getId_product() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }
}
