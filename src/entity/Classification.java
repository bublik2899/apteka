package entity;

import java.io.Serializable;

public class Classification implements Serializable {

    private int id;
    private String name;

    public Classification(String name){
        this.name = name;
    }


    public Classification(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getIdClassification(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
