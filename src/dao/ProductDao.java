package dao;

import entity.Classification;
import entity.Product;
import database.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;

public class ProductDao {
    ClassificationDao classificationDao = new ClassificationDao();

    public  ArrayList<Product> selectAll() {
        ArrayList<Product> products = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM apteka.product")) {
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                int amount = resultSet.getInt(3);
                int classification = resultSet.getInt(4);
                Classification classif = classificationDao.selectOne(classification);
                Product product = new Product(id, name, amount, classif);
                products.add(product);
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return products;
    }

    public  void insert(Product product) {
        String sql = "INSERT INTO apteka.product (name, amount, classification) Values (?, ?, ?)";
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setInt(2, product.getAmount());
            Classification classification = product.getClassification();
            preparedStatement.setInt(3, classification.getIdClassification());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public  void delete(int id) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        String sql = "DELETE FROM apteka.product WHERE id_product=?";
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public  Product selectOne(int id) {
        ConnectionPool pool = ConnectionPool.getInstance();
        String sql = "SELECT * FROM apteka.product WHERE id_product=?";
        try (Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int id1 = resultSet.getInt(1);
                String name = resultSet.getString(2);
                int amount = resultSet.getInt(3);
                int classif = resultSet.getInt(4);
                Classification classification = null;
                ClassificationDao dao = new ClassificationDao();
                classification = dao.selectOne(classif);
                return new Product(id1, name, amount, classification);

            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public  ArrayList<Product> findByClassification(int id) {
        ArrayList<Product> products = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        String sql = "SELECT * from apteka.product INNER JOIN apteka.classification on product.classification=id_classification where id_classification = ?";
        try (Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            Product product;
            while (resultSet.next()) {
                int id1 = resultSet.getInt(1);
                String name = resultSet.getString(2);
                int amount = resultSet.getInt(3);
                int id2 = resultSet.getInt(5);
                String name2 = resultSet.getString(6);
                Classification classification2;
                classification2 = new Classification(id2, name2);
                product = new Product(id1, name, amount, classification2);
                products.add(product);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public  void update(Product product) {
        ConnectionPool pool = ConnectionPool.getInstance();
        String sql = "UPDATE apteka.product SET name=?, amount=?, classification=? WHERE id_product=?";
        try (   Connection connection = pool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setInt(2, product.getAmount());
            Classification classification = product.getClassification();
            preparedStatement.setInt(3, classification.getIdClassification());
            preparedStatement.setLong(4, product.getId_product());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Product> findByName(String name) {
        ConnectionPool pool = ConnectionPool.getInstance();
        String sql = "SELECT * FROM apteka.product WHERE product.name LIKE ? ";
        ArrayList<Product> products = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name1 = resultSet.getString(2);
                int amount = resultSet.getInt(3);
                int idClassification = resultSet.getInt(4);
                Classification classification = classificationDao.selectOne(idClassification);
                Product product = new Product(id, name1, amount, classification);
                products.add(product);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return products;
    }

    public ArrayList<Product> findByNameAndClassification(String name, int id) {
        ConnectionPool pool = ConnectionPool.getInstance();
        ArrayList<Product> products = new ArrayList<>();
        String sql = "SELECT * FROM apteka.product WHERE product.name LIKE ? AND product.classification=?";
        try (Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, "%" + name + "%");
            preparedStatement.setInt(2, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id1 = resultSet.getInt(1);
                String name1 = resultSet.getString(2);
                int amount = resultSet.getInt(3);
                int idClassification = resultSet.getInt(4);
                Classification classification = classificationDao.selectOne(idClassification);
                Product product = new Product(id1, name1, amount, classification);
                products.add(product);

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }
}





