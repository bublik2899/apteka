package dao;

import entity.Classification;
import database.ConnectionPool;


import java.sql.*;
import java.util.ArrayList;

public class ClassificationDao {

    public  ArrayList<Classification> selectAll() {
        ArrayList<Classification> classifications = new ArrayList<Classification>();
        ConnectionPool pool = ConnectionPool.getInstance();
        try (Connection connection = pool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM apteka.classification")) {
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);

                Classification classification = new Classification(id, name);
                classifications.add(classification);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return classifications;
    }

    public  Classification selectOne(int id) {
        ResultSet resultSet = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        String sql = "SELECT * FROM apteka.classification WHERE id_classification=?";
        try (Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {

            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                int classif_id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                return new Classification(classif_id, name);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
/*Объект ResultSet автоматически закрывается объектом Statement, который сгенерировал его, когда этот объект Statement закрыт, повторно выполняется или используется для извлечения следующего результата из последовательности нескольких результатов.*/

