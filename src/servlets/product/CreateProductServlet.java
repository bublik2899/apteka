package servlets.product;

import entity.Classification;
import entity.Product;
import dao.ClassificationDao;
import dao.ProductDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/product/create")
public class CreateProductServlet extends HttpServlet {

    ClassificationDao classificationDao = new ClassificationDao();
    ProductDao productDao = new ProductDao();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Classification> classifications = classificationDao.selectAll();
        request.setAttribute("classific",classifications);
        getServletContext().getRequestDispatcher("/product/create.jsp").forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String name = request.getParameter("name");
            int amount = Integer.parseInt(request.getParameter("amount"));
            int idClassific = Integer.parseInt(request.getParameter("classific"));
            Classification classification = classificationDao.selectOne(idClassific);
            Product product = new Product(name, amount, classification);
            productDao.insert(product);
            response.sendRedirect(request.getContextPath()+"/product");
        }
        catch(Exception ex) {
            ex.printStackTrace();
            getServletContext().getRequestDispatcher("product/create.jsp").forward(request, response);
        }
    }
}
