package servlets.product;

import entity.Classification;
import entity.Product;
import dao.ClassificationDao;
import dao.ProductDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/product/search")
public class SearchandSortProductServlet extends HttpServlet {

    ClassificationDao classificationDao = new ClassificationDao();
    ProductDao productDao = new ProductDao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String check1 = request.getParameter("check1");
        String check2 = request.getParameter("check2");
        int id = Integer.parseInt(request.getParameter("classific"));
        ArrayList<Classification> classifications = classificationDao.selectAll();
        request.setAttribute("classifications", classifications);

        try {
            if (check1 != null && check2 == null) {
                String name = request.getParameter("name");

                ArrayList<Product> products = productDao.findByName(name);

                if (products.isEmpty()) {
                    getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
                } else {
                    request.setAttribute("products", products);
                    getServletContext().getRequestDispatcher("/product/product.jsp").forward(request, response);
                }
            }
            if(check1 !=null && check2 != null){
                String name = request.getParameter("name");
                ArrayList<Product> products =  productDao.findByNameAndClassification(name,id);
                if (products.isEmpty()) {
                    getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
                } else {
                    request.setAttribute("products", products);
                    getServletContext().getRequestDispatcher("/product/product.jsp").forward(request, response);
                }
            }
            else {
                ArrayList<Product> products = productDao.findByClassification(id);
                if (products.isEmpty()) {
                    getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
                } else {
                    request.setAttribute("products", products);
                    getServletContext().getRequestDispatcher("/product/product.jsp").forward(request, response);
                }
            }
        } catch (ServletException e) {
            e.printStackTrace();
        }

    }
}
