package servlets.product;

import entity.Classification;
import entity.Product;
import dao.ClassificationDao;
import dao.ProductDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class ProductServlet extends HttpServlet {
    ClassificationDao classificationDao = new ClassificationDao();
    ProductDao productDao = new ProductDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Product> products = productDao.selectAll();
        req.setAttribute("products", products);
        ArrayList<Classification> classifications = classificationDao.selectAll();
        req.setAttribute("classifications", classifications);
        try {
            getServletContext().getRequestDispatcher("/product/product.jsp").forward(req, resp);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
