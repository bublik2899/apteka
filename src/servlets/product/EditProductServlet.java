package servlets.product;

import entity.Classification;
import entity.Product;
import dao.ClassificationDao;
import dao.ProductDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/product/edit")
public class EditProductServlet extends HttpServlet {
    ClassificationDao classificationDao = new ClassificationDao();
    ProductDao productDao = new ProductDao();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            int amount = Integer.parseInt(request.getParameter("amount"));
            int idClassif = Integer.parseInt(request.getParameter("classific"));
            Classification classification = classificationDao.selectOne(idClassif);
            Product product = new Product(id, name, amount, classification);
            productDao.update(product);
            response.sendRedirect(request.getContextPath() + "/product");
        }
        catch(Exception ex) {

            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            int id = Integer.parseInt(request.getParameter("id"));
            Product product = productDao.selectOne(id);
            ArrayList<Classification> classifications = classificationDao.selectAll();
            request.setAttribute("classific" , classifications);
            if(product!=null) {
                request.setAttribute("product", product);
                getServletContext().getRequestDispatcher("/product/edit.jsp").forward(request, response);
            }
            else {
                getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
            }
        }
        catch (ServletException e){
            e.printStackTrace();
        }


    }
}
