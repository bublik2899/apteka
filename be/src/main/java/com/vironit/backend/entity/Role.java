package com.vironit.backend.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Role {
    private short idRole;
    private String nameRole;

    @Id
    @Column(name = "id_role", nullable = false)
    public short getIdRole() {
        return idRole;
    }

    public void setIdRole(short idRole) {
        this.idRole = idRole;
    }

    @Basic
    @Column(name = "name_role", nullable = true, length = 45)
    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (idRole != role.idRole) return false;
        if (nameRole != null ? !nameRole.equals(role.nameRole) : role.nameRole != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) idRole;
        result = 31 * result + (nameRole != null ? nameRole.hashCode() : 0);
        return result;
    }
}
