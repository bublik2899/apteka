package com.vironit.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Entity
public class Product {
    private Integer idProduct;
    private String name;
    private Integer amount;
    private String description;
    private Integer price;
    private Classification classificationByClassification;
    //private Set<BucketOrder> orders;
    private Integer currentAmount;

    /*@OneToMany(mappedBy = "product")
    public Set<BucketOrder> getOrders() {
        return orders;
    }

    public void setOrders(Set<BucketOrder> orders) {
        this.orders = orders;
    }*/

    @Id
    @Column(name = "id_product", nullable = false)
    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }


    @Basic
    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Transient
    public Integer getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Integer currentAmount) {
        this.currentAmount = currentAmount;
    }

    @Basic
    @Column(name = "amount", nullable = true)
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "price", nullable = true)
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (!idProduct.equals(product.idProduct)) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (amount != null ? !amount.equals(product.amount) : product.amount != null) return false;
        if (description != null ? !description.equals(product.description) : product.description != null) return false;
        return price != null ? price.equals(product.price) : product.price == null;
    }

    @Override
    public int hashCode() {
        int result = idProduct;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }





    @ManyToOne
    @JoinColumn(name = "classification", referencedColumnName = "id_classification")
    public Classification getClassificationByClassification() {
        return classificationByClassification;
    }

    public void setClassificationByClassification(Classification classificationByClassification) {
        this.classificationByClassification = classificationByClassification;
    }

    /*@ManyToMany(mappedBy = "products")
    public Set<Order> getOrders(){
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }*/
}
