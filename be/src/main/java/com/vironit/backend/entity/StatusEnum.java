package com.vironit.backend.entity;

public enum StatusEnum {

    created,
    approved,
    shipped,
    undefined,
    finshed;

    StatusEnum() {
    }

}