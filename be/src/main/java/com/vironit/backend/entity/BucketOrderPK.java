package com.vironit.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable

public class BucketOrderPK implements Serializable {

    private Integer idOrder;
    private Integer idProduct;

    public BucketOrderPK(){}

    public BucketOrderPK(Integer idOrder, Integer idProduct) {
        this.idOrder = idOrder;
        this.idProduct = idProduct;
    }

    @Column(name = "id_order", nullable = false)
    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }


    @Column(name = "id_product", nullable = false)
    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BucketOrderPK that = (BucketOrderPK) o;

        if (idOrder != null ? !idOrder.equals(that.idOrder) : that.idOrder != null) return false;
        return idProduct != null ? idProduct.equals(that.idProduct) : that.idProduct == null;
    }

    public int hashCode() {
        int result;
        result = (idOrder != null ? idOrder.hashCode() : 0);
        result = 31 * result + (idProduct != null ? idProduct.hashCode() : 0);
        return result;
    }

}
