package com.vironit.backend.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
@TypeDef(
        name = "request_state",
        typeClass = ConverType.class
)

public class Order {

    private LocalDate date;
    private String shippingAdress;
    private String description;
    private int id;
    private Set<Product> products = new HashSet<>();
    private Set<BucketOrder> amount;
    private int finalPrice;
    private StatusEnum status;
    private User user1;
    private User employee;

    @Basic
    @Column(name = "final_price")
    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    @Basic
    @Column(name = "date", nullable = true)
    @JsonFormat(pattern="dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Basic
    @Column(name = "shipping_adress", nullable = true, length = 40)
    public String getShippingAdress() {
        return shippingAdress;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "status")
    @Type( type = "request_state" )
    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public void setShippingAdress(String shippingAdress) {
        this.shippingAdress = shippingAdress;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 40)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (date != null ? !date.equals(order.date) : order.date != null) return false;
        if (shippingAdress != null ? !shippingAdress.equals(order.shippingAdress) : order.shippingAdress != null)
            return false;
        return description != null ? description.equals(order.description) : order.description == null;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (shippingAdress != null ? shippingAdress.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }

    @OneToMany(mappedBy = "order")
    public Set<BucketOrder> getAmount() {
        return amount;
    }

    public void setAmount(Set<BucketOrder> amount) {
        this.amount = amount;
    }


    @ManyToOne
    @JoinColumn(name = "user1", referencedColumnName = "id_user", nullable = false)
    public User getUser() {
        return user1;
    }

    public void setUser(User user1) {
        this.user1 = user1;
    }


    @ManyToOne
    @JoinColumn(name = "employee", referencedColumnName = "id_user", nullable = false)
    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }
}
