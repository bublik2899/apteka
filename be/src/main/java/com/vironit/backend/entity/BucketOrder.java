package com.vironit.backend.entity;



import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class BucketOrder {


    @EmbeddedId
    private  BucketOrderPK bucketOrderPK;

    public BucketOrder(){};

    public BucketOrder(Order order, Product product, Integer amount) {
        this.bucketOrderPK = new BucketOrderPK(order.getId(), product.getIdProduct());
        this.order = order;
        this.product = product;
        this.amount = amount;
    }


    @JsonIgnore
    @MapsId("idOrder")
    @ManyToOne
    @JoinColumn(name = "id_order", referencedColumnName = "id",insertable = false, updatable = false)
    private Order order;

    @MapsId("idProduct")
    @ManyToOne
    @JoinColumn(name = "id_product", referencedColumnName = "id_product",insertable = false, updatable = false)
    private Product product;

    @Basic
    @Column(name = "amount", nullable = true)
    private Integer amount;

    @JsonIgnore
    public BucketOrderPK getBucketOrderPK() {
        return bucketOrderPK;
    }

    public void setBucketOrderPK(BucketOrderPK bucketOrderPK) {
        this.bucketOrderPK = bucketOrderPK;
    }

    public Order getOrder(){
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }


    public Product getProduct(){
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BucketOrder that = (BucketOrder) o;

        if (product != that.product) return false;
        if (order != that.order) return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (product != null ? product.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        return result;
    }


}
