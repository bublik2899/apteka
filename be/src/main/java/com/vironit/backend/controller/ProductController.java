package com.vironit.backend.controller;

import com.vironit.backend.service.ProductService;
import com.vironit.backend.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@RestController
@RequestMapping("/be/apteka/product/")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public List<Product> getAllProduct(@RequestParam(value = "numberPage") int numberPage){
        return productService.allProducts(numberPage);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<Product> getProductByParam(@RequestParam(value = "name") String name,
                                          @RequestParam(value = "id") Integer id,
                                          @RequestParam(value = "min") Integer minPrice,
                                          @RequestParam(value = "max") Integer maxPrice){
        if(id == 0) id = null;
        return productService.findProduct(name,id,minPrice,maxPrice);
    }



    @RequestMapping(value = "/getbyid/{id}", method = RequestMethod.GET)
    public Product getProductById(@PathVariable(name = "id") Integer id){
        return productService.getProductById(id);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.GET)
    public List<Product> sortProduct(@RequestParam(name = "id") Integer idClassif,
                                     @RequestParam(name = "asc") Boolean asc,
                                     @RequestParam(name = "pricemin") Boolean priceAsc,
                                     @RequestParam(name = "pricemax") Boolean priceDesc){
        if(idClassif == 0) idClassif = null;
        return productService.sortProduct(idClassif, asc, priceAsc, priceDesc);
    }

}