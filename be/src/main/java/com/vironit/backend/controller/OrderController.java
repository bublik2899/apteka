package com.vironit.backend.controller;


import com.vironit.backend.dto.OrderDto.OrderDto1;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.Product;
import com.vironit.backend.entity.StatusEnum;
import com.vironit.backend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/be/apteka/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.POST)
    public void saveOrder(@RequestBody Order order) {

        orderService.saveOrder(order);
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public List<OrderDto1> getAll(){

        return orderService.getAll();
    }

    @RequestMapping(value = "/order" , method = RequestMethod.POST)
    public void updateOrder(@RequestBody Order order){
        orderService.updateOrder(order);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<OrderDto1> findOrders(@RequestParam(value = "start")
                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                      @RequestParam(value = "end")
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end,
                                      @RequestParam(value = "status") StatusEnum status,
                                      @RequestParam(value = "last") String lastname,
                                      @RequestParam(value = "first") String firstname){

        if(status == StatusEnum.undefined){
            status = null;
        }


        return orderService.findOrder(start, end,  status, lastname, firstname);

    }

}
