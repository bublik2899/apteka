package com.vironit.backend.controller;


import com.vironit.backend.entity.Classification;
import com.vironit.backend.service.ClassificationService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/be/apteka/classification")
public class ClassificationController {

    private final ClassificationService classificationService;

    @Autowired
    public  ClassificationController(ClassificationService classificationService){
        this.classificationService = classificationService;
    }

    @RequestMapping(value = "/classifications", method = RequestMethod.GET)
    public List<Classification> getAllClassification(){

        return classificationService.allClassification();

    }
}
