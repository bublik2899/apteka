package com.vironit.backend.controller;


import com.vironit.backend.dto.UserDto.UserDto;
import com.vironit.backend.entity.User;
import com.vironit.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/be/apteka/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/getbyemail", method = RequestMethod.GET)
    public UserDto getUserByEmail(@RequestParam(value = "email") String email){
        return userService.getUserByEmail(email);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void saveUser(@RequestBody User user) {

        userService.saveUser(user);
    }
}
