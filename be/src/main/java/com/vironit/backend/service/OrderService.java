package com.vironit.backend.service;

import com.vironit.backend.dto.OrderDto.OrderDto1;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.StatusEnum;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public interface OrderService {

    void saveOrder(Order order);

    List<OrderDto1> getAll();

    void updateOrder(Order order);

    List<OrderDto1> findOrder(LocalDate start, LocalDate end, StatusEnum status, String lastname, String firstname);

}
