package com.vironit.backend.service;

import com.vironit.backend.dto.UserDto.UserDto;
import com.vironit.backend.entity.User;

public interface UserService {

    public UserDto getUserByEmail(String email);

    public void saveUser(User user);

}
