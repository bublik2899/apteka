package com.vironit.backend.service;

import com.vironit.backend.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findProduct(String name, Integer id, Integer minPrice, Integer maxPrice);

    List<Product> allProducts(int numberPage);

    Product getProductById(Integer id);

    List<Product> sortProduct(Integer idClassif, Boolean asc, Boolean priceAsc, Boolean priceDesc);

}
