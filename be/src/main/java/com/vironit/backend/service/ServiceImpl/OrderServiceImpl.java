package com.vironit.backend.service.ServiceImpl;

import com.vironit.backend.dao.OrderDao;
import com.vironit.backend.dto.OrderDto.OrderDto1;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.StatusEnum;
import com.vironit.backend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    @Transactional
    public void saveOrder(Order order) {

        orderDao.saveOrder(order);
    }

    @Transactional
    @Override
    public List<OrderDto1> getAll()
    {
        return orderDao.findAll();
    }

    @Transactional
    @Override
    public void updateOrder(Order order){

        orderDao.updateOrder(order);

    }

    @Transactional
    @Override
    public List<OrderDto1> findOrder(LocalDate start, LocalDate end, StatusEnum status, String lastname, String firstname) {
        return orderDao.findOrder(start, end, status, lastname, firstname);
    }
}
