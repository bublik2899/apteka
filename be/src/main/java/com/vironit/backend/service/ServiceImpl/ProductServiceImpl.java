package com.vironit.backend.service.ServiceImpl;


import com.vironit.backend.dao.ProductDao;
import com.vironit.backend.service.ProductService;
import com.vironit.backend.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService  {


    private final ProductDao productDao;

    @Autowired
    public ProductServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Transactional
    public List<Product> findProduct(String name, Integer id, Integer minPrice, Integer maxPrice){

        return productDao.findProduct(name, id, minPrice, maxPrice);
    }

    @Transactional
    public List<Product> allProducts(int numberPage) {

        return  productDao.allProducts(numberPage);
    }



    @Transactional
    public List<Product> sortProduct(Integer idClassif, Boolean asc, Boolean priceAsc, Boolean priceDesc) {
        return productDao.sortProduct(idClassif, asc, priceAsc, priceDesc);
    }



    @Transactional
    @Override
    public Product getProductById(Integer id) {
        return productDao.findProductById(id);
    }
}
