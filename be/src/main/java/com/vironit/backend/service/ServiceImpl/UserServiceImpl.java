package com.vironit.backend.service.ServiceImpl;

import com.vironit.backend.dao.UserDao;
import com.vironit.backend.dto.UserDto.UserDto;
import com.vironit.backend.entity.User;
import com.vironit.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Transactional
    @Override
    public UserDto getUserByEmail(String email) {

        return userDao.findUserByEmail(email);
    }

    @Transactional
    @Override
    public void saveUser(User user){
        userDao.saveUser(user);
    }
}
