package com.vironit.backend.service.ServiceImpl;

import com.vironit.backend.dao.ClassificationDao;
import com.vironit.backend.entity.Classification;
import com.vironit.backend.service.ClassificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClassificationServiceImpl implements ClassificationService {

    private final ClassificationDao classificationDao;

    @Autowired
    public ClassificationServiceImpl(ClassificationDao classificationDao) {
        this.classificationDao = classificationDao;
    }

    @Transactional
    public List<Classification> allClassification(){

        List<Classification>  c= classificationDao.allClassification();

        return c;
    }
}
