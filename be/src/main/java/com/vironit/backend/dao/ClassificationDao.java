package com.vironit.backend.dao;

import com.vironit.backend.entity.Classification;

import java.util.List;

public interface ClassificationDao {

    List<Classification> allClassification();
}
