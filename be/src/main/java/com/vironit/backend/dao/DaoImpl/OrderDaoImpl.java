package com.vironit.backend.dao.DaoImpl;

import com.vironit.backend.dao.OrderDao;
import com.vironit.backend.dto.OrderDto.OrderDto1;
import com.vironit.backend.dto.UserDto.OrderDto;
import com.vironit.backend.entity.*;
import com.vironit.backend.entity.Order;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public class OrderDaoImpl implements OrderDao {

    private final SessionFactory sessionFactory;
    Query query;

    @Autowired
    public OrderDaoImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }


    @Override
    public void saveOrder(Order order) {
        Session session = sessionFactory.getCurrentSession();
        Set<Product> s = order.getProducts();
        order.setDate(LocalDate.now());
        order.setStatus(StatusEnum.created);
        User manager = new User();
        manager.setIdUser(34);
        order.setEmployee(manager);
        session.persist(order);
        for(Product p : s){
            p.setAmount(p.getAmount() - p.getCurrentAmount());
            session.update(p);
            BucketOrder bucketOrder = new BucketOrder(order,p , p.getCurrentAmount()); //persist то ошибка совпадения id product?????
            session.save(bucketOrder);
        }

    }

    @Override
    public void updateOrder(Order order){
        Session session = sessionFactory.getCurrentSession();
        session.update(order);
    }

    @Override
    public List<OrderDto1> findAll() {

        Session session = sessionFactory.getCurrentSession();
        query = session.createQuery("from Order");
        ArrayList<Order> l = (ArrayList) query.list();
        ArrayList<OrderDto1> l2 = new ArrayList<>();
        for(Order o : l ) {
            Hibernate.initialize(o.getUser());
            Hibernate.initialize(o.getAmount());
            l2.add(OrderDto1.fromEntiyOrder(o));
        }
        return l2;
    }

    @Override
    public List<OrderDto1> findOrder(LocalDate start, LocalDate end, StatusEnum status, String lastname, String firstname)

    {

        String lastN = "%" + lastname + "%";
        String firstN = "%" + firstname + "%";
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Order> cr = cb.createQuery(Order.class);
        Root<Order> root = cr.from(Order.class);
        Join<Order, User> users = root.join("user");
        cr.select(root);

        Predicate criteria = cb.conjunction();


        if (start == null) {
            start = LocalDate.of(2000, 1, 1);
        }

        if (end == null) {
            end = LocalDate.now().plusDays(1);
        }

        Predicate p1 = cb.between(root.get("date"), start, end);
        criteria = cb.and(criteria, p1);

        if (status != null) {
            Predicate p = cb.equal(root.get("status"), status);
            criteria = cb.and(criteria, p);

        }


        if (lastname != null) {
            Predicate p = cb.like(users.get("lastname"), lastN);
            criteria = cb.and(criteria, p);
        }


        if (firstname != null) {
            Predicate p = cb.like(users.get("firstname"), firstN);
            criteria = cb.and(criteria, p);
        }


        List<Order> list = session.createQuery(cr.where(criteria)).getResultList();

        List<OrderDto1> orders = new ArrayList<>();

        for (Order o : list) {

            Hibernate.initialize(o.getAmount());
            orders.add(OrderDto1.fromEntiyOrder(o));

        }

        return orders;

    }

}
