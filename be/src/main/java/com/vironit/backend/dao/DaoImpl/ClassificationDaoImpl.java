package com.vironit.backend.dao.DaoImpl;


import com.vironit.backend.dao.ClassificationDao;
import com.vironit.backend.entity.Classification;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClassificationDaoImpl implements ClassificationDao {

    private final SessionFactory sessionFactory;

    @Autowired
    public ClassificationDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Classification> allClassification() {
        Session session = sessionFactory.getCurrentSession();

        Query query= session.createSQLQuery("SELECT * FROM apteka.classification").addEntity(Classification.class);
        List<Classification> c = query.list();
        return c;
    }
}