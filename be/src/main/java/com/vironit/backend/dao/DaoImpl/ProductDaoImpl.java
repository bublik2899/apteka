package com.vironit.backend.dao.DaoImpl;

import com.vironit.backend.dao.ProductDao;
import com.vironit.backend.entity.Classification;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.Product;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {

    private Query query;

    private SessionFactory sessionFactory;

    @Autowired
    public void SessionFactory(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Product> findProduct(String name, Integer idClassif, Integer minPrice, Integer maxPrice) {

        if (minPrice == null) minPrice = 0;
        if (maxPrice == null) maxPrice = 100000;
        String s = "%" + name + "%";
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Product> cr = cb.createQuery(Product.class);
        Root<Product> root = cr.from(Product.class);
        Join<Product, Classification> classification = root.join("classificationByClassification");
        cr.select(root);


        Predicate criteria = cb.conjunction();

        if (name != null) {
            Predicate p = cb.like(root.<String>get("name"), s);
            criteria = cb.and(criteria, p);
        }

        if (idClassif != null) {
            Predicate p = cb.equal(classification.get("idClassification"), idClassif);
            criteria = cb.and(criteria, p);
        }

        Predicate p = cb.between(root.<Integer>get("price"), minPrice, maxPrice);
        criteria = cb.and(criteria, p);

        query = session.createQuery((cr.where(criteria)));
        return query.getResultList();
    }


    @Override
    public List<Product> sortProduct(Integer idClassif, Boolean asc, Boolean priceAsc, Boolean priceDesc)

    {

        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Product> cr = cb.createQuery(Product.class);
        Root<Product> root = cr.from(Product.class);
        Join<Product, Classification> classification = root.join("classificationByClassification");
        cr.select(root);


        Predicate criteria = cb.conjunction();

        if (idClassif != null) {
            Predicate p = cb.equal(classification.get("idClassification"), idClassif);
            criteria = cb.and(criteria, p);
        }

        cr.where(criteria);

        if (asc) {
            return session.createQuery(cr.orderBy(cb.asc(root.get("name")))).getResultList();
        }

        if (priceAsc) {
            return session.createQuery(cr.orderBy(cb.asc(root.get("price")))).getResultList();
        }

        if (priceDesc) {
            return session.createQuery(cr.orderBy(cb.desc(root.get("price")))).getResultList();
        }

        query = session.createQuery(cr.where(criteria));

        return query.getResultList();

    }





    @Override
    public List<Product> allProducts(int numberPage) {
        Session session = sessionFactory.getCurrentSession();
        query = session.createQuery("from Product ");
        query = paginationProducts(numberPage, query);
        return query.getResultList();
    }



    @Override
    public Product findProductById(Integer id){
        Session session = sessionFactory.getCurrentSession();
        query = session.createQuery("from Product where idProduct = :paramId");
        query.setParameter("paramId", id);
        return (Product) query.getSingleResult();
    }



    private Query paginationProducts(int numberPage , Query query){

        byte max = 7;

        query.setFirstResult((numberPage-1)*7);
        query.setMaxResults(7);

        return query;
    }

}
