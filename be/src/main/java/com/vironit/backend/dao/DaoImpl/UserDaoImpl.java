package com.vironit.backend.dao.DaoImpl;

import com.vironit.backend.dao.UserDao;
import com.vironit.backend.dto.UserDto.UserDto;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.User;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Set;


@Repository
public class UserDaoImpl implements UserDao {

    Query query;

    private SessionFactory sessionFactory;

    @Autowired
    public void SessionFactory(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserDto findUserByEmail(String email) {
        try {
            Session session = sessionFactory.getCurrentSession();
            query = session.createQuery("from User where email = :paramName");
            query.setParameter("paramName", email);
            User user1 = (User) query.getSingleResult();
            Set<Order> orders = user1.getOrders();
            Hibernate.initialize(user1.getOrders());
            for (Order o : orders) {
                Hibernate.initialize(o.getAmount());
            }
            return UserDto.fromEntity(user1);

        }
        catch (NoResultException e){
            return new UserDto();
        }
    }

    @Override
    public void saveUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(user);
    }
}
