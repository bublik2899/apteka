package com.vironit.backend.dao;

import com.vironit.backend.entity.Product;

import java.util.List;

public interface ProductDao {

    List<Product> findProduct(String name, Integer idClassif, Integer minPrice, Integer maxPrice);

    List<Product> allProducts(int numberPage);

    Product findProductById(Integer id);

    List<Product> sortProduct(Integer idClassif, Boolean asc, Boolean priceAsc, Boolean priceDesc);

}
