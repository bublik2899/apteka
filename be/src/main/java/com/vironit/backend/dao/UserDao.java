package com.vironit.backend.dao;

import com.vironit.backend.dto.UserDto.UserDto;
import com.vironit.backend.entity.User;

public interface UserDao {

    public UserDto findUserByEmail(String email);

    public void saveUser(User user);

}
