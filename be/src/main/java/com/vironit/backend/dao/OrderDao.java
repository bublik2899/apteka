package com.vironit.backend.dao;

import com.vironit.backend.dto.OrderDto.OrderDto1;
import com.vironit.backend.dto.UserDto.OrderDto;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.StatusEnum;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public interface OrderDao {

    void saveOrder(Order order);

    List<OrderDto1> findAll();

    void updateOrder(Order order);

    List<OrderDto1> findOrder(LocalDate start, LocalDate end, StatusEnum status, String lastname, String firstname);

}
