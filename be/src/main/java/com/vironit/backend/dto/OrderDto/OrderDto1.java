package com.vironit.backend.dto.OrderDto;

import com.vironit.backend.entity.BucketOrder;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.StatusEnum;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class OrderDto1 {

    private LocalDate date;
    private String shippingAdress;
    private String description;
    private Set<BucketOrder> productsTransf = new HashSet<>();
    private int finalPrice;
    private StatusEnum status;
    private UserDto1 user;
    private Integer id;
    private UserDto1 employee;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getShippingAdress() {
        return shippingAdress;
    }

    public void setShippingAdress(String shippingAdress) {
        this.shippingAdress = shippingAdress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<BucketOrder> getProductsTransf() {
        return productsTransf;
    }

    public void setProductsTransf(Set<BucketOrder> productsTransf) {
        this.productsTransf = productsTransf;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public UserDto1 getUser() {
        return user;
    }

    public void setUser(UserDto1 user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDto1 getEmployee() {
        return employee;
    }

    public void setEmployee(UserDto1 employee) {
        this.employee = employee;
    }

    public static OrderDto1 fromEntiyOrder(Order order){
        OrderDto1 orderDto = new OrderDto1();
        orderDto.setDate(order.getDate());
        orderDto.setDescription(order.getDescription());
        orderDto.setShippingAdress(order.getShippingAdress());
        orderDto.setFinalPrice(order.getFinalPrice());
        orderDto.setStatus(order.getStatus());
        orderDto.setProductsTransf(order.getAmount());
        orderDto.setUser(UserDto1.fromEntity(order.getUser()));
        orderDto.setId(order.getId());
        orderDto.setEmployee(UserDto1.fromEntity(order.getEmployee()));
        return orderDto;
    }
}
