package com.vironit.backend.dto.UserDto;

import com.vironit.backend.entity.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class OrderDto {

    private LocalDate date;
    private String shippingAdress;
    private String description;
    private Set<BucketOrder> productsTransf = new HashSet<>();
    private int finalPrice;
    private StatusEnum status;
    private Integer id;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getShippingAdress() {
        return shippingAdress;
    }

    public void setShippingAdress(String shippingAdress) {
        this.shippingAdress = shippingAdress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<BucketOrder> getProductsTransf() {
        return productsTransf;
    }

    public void setProductsTransf(Set<BucketOrder> productsTransf) {
        this.productsTransf = productsTransf;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public static OrderDto fromEntiyOrder(Order order){
        OrderDto orderDto = new OrderDto();
        orderDto.setDate(order.getDate());
        orderDto.setDescription(order.getDescription());
        orderDto.setShippingAdress(order.getShippingAdress());
        orderDto.setFinalPrice(order.getFinalPrice());
        orderDto.setStatus(order.getStatus());
        orderDto.setProductsTransf(order.getAmount());
        orderDto.setId(order.getId());
        return orderDto;
    }
}
