package com.vironit.backend.dto.UserDto;

import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.Role;
import com.vironit.backend.entity.User;

import java.util.HashSet;
import java.util.Set;

public class UserDto {

    private int idUser;
    private String lastname;
    private String firstname;
    private String phoneNumber;
    private String email;
    private String password;
    private Role roleByRole;

    public Role getRoleByRole() {
        return roleByRole;
    }

    public void setRoleByRole(Role roleByRole) {
        this.roleByRole = roleByRole;
    }

    private Set<OrderDto> orders = new HashSet<>();

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(Set<OrderDto> orders) {
        this.orders = orders;
    }



    public static UserDto fromEntity(User user){

        UserDto userDto = new UserDto();
        userDto.setIdUser(user.getIdUser());
        userDto.setFirstname(user.getFirstname());
        userDto.setLastname(user.getLastname());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setPhoneNumber(user.getPhoneNumber());
        Set<OrderDto> orderDtos = new HashSet<>();
        for(Order order : user.getOrders()){
            orderDtos.add(OrderDto.fromEntiyOrder(order));
        }
        userDto.setRoleByRole(user.getRoleByRole());
        userDto.setOrders(orderDtos);
        return userDto;
    }
}
