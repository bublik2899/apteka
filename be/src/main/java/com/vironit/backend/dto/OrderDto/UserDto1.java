package com.vironit.backend.dto.OrderDto;

import com.vironit.backend.dto.UserDto.OrderDto;
import com.vironit.backend.dto.UserDto.UserDto;
import com.vironit.backend.entity.Order;
import com.vironit.backend.entity.User;

import java.util.HashSet;
import java.util.Set;

public class UserDto1 {

    private int idUser;
    private String lastname;
    private String firstname;
    private String phoneNumber;
    private String email;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static UserDto1 fromEntity(User user){

        UserDto1 userDto = new UserDto1();
        userDto.setIdUser(user.getIdUser());
        userDto.setFirstname(user.getFirstname());
        userDto.setLastname(user.getLastname());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        return userDto;
    }
}
